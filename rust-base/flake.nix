{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-analyzer-src.follows = "";
    };

    crane.url = "github:ipetkov/crane";

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };
  };

  outputs = inputs @ {...}: let
    system = "x86_64-linux";
    overlays = [inputs.fenix.overlays.default];

    pkgs = import inputs.nixpkgs {
      inherit system overlays;
    };

    advisory-db = inputs.advisory-db;

    # Rust overlay channel to be used
    fenixChannel = inputs.fenix.packages.${system}.latest;

    # Rust toolchain components from overlay channel
    fenixToolchain = fenixChannel.withComponents [
      "cargo"
      "clippy"
      "rustc"
      "rustfmt"
      "rustc-codegen-cranelift"
    ];

    # NB: we don't need to overlay our custom toolchain for the *entire*
    # pkgs (which would require rebuidling anything else which uses rust).
    # Instead, we just want to update the scope that crane will use by appending
    # our specific toolchain there.
    craneLib = (inputs.crane.mkLib pkgs).overrideToolchain (p: fenixToolchain);

    src = craneLib.cleanCargoSource ./.;

    commonArgs = {
      inherit src;

      buildInputs = [
        # Add additional build inputs here
        pkgs.clang
        pkgs.mold-wrapped
        pkgs.openssl
        pkgs.pkg-config
      ];

      # Additional environment variables can be set directly
      # MY_CUSTOM_VAR = "some value";
    };

    # Build *just* the cargo dependencies, so we can reuse
    # all of that work (e.g. via cachix) when running in CI
    cargoArtifacts = craneLib.buildDepsOnly commonArgs;

    # Build the actual crate itself, reusing the dependency
    # artifacts from above.
    my-crate = craneLib.buildPackage (commonArgs
      // {
        inherit cargoArtifacts;
      });
  in {
    checks.${system} = {
      # Build the crate as part of `nix flake check` for convenience
      inherit my-crate;

      # Run clippy (and deny all warnings) on the crate source,
      # again, reusing the dependency artifacts from above.
      #
      # Note that this is done as a separate derivation so that
      # we can block the CI if there are issues here, but not
      # prevent downstream consumers from building our crate by itself.
      my-crate-clippy = craneLib.cargoClippy (commonArgs
        // {
          inherit cargoArtifacts;
          cargoClippyExtraArgs = "--all-targets -- --deny warnings";
        });

      my-crate-doc = craneLib.cargoDoc (commonArgs
        // {
          inherit cargoArtifacts;
        });

      # Check formatting
      my-crate-fmt = craneLib.cargoFmt {
        inherit src;
      };

      # Check toml formatting
      my-crate-toml-fmt = craneLib.taploFmt {
        src = pkgs.lib.sources.sourceFilesBySuffices src [".toml"];
        # taplo arguments can be further customized below as needed
        taploExtraArgs = "--config ./taplo.toml";
      };

      # Audit dependencies
      my-crate-audit = craneLib.cargoAudit {
        inherit src advisory-db;
      };

      # Audit licenses
      my-crate-deny = craneLib.cargoDeny {
        inherit src;
      };

      # Run tests with cargo-nextest
      # Consider setting `doCheck = false` on `my-crate` if you do not want
      # the tests to run twice
      my-crate-nextest = craneLib.cargoNextest (commonArgs
        // {
          inherit cargoArtifacts;
          partitions = 1;
          partitionType = "count";
        });
    };

    packages.${system}.default = my-crate;

    devShells.${system}.default = craneLib.devShell {
      # Automatically inherit any build inputs from `my-crate`
      inputsFrom = [my-crate];

      # Extra inputs (only used for interactive development)
      packages = [
        pkgs.bacon
        pkgs.just
      ];

      # Additional dev-shell environment variables can be set directly
      # MY_CUSTOM_DEV_URL = "http://localhost:3000";
    };
  };
}
