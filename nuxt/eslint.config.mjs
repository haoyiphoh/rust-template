// @ts-check
import withNuxt from './.nuxt/eslint.config.mjs'

export default withNuxt().override('nuxt/typescript/rules', {
  // rules: {
  //   // ...Override rules, for example:
  //   '@typescript-eslint/no-unused-vars': 'error',
  // },
})
